#!/usr/bin/make -f
# Copyright © 2010 Hans-Christoph Steiner <hans@eds.org>
# Copyright © 2018 IOhannes m zmölnig <umlaeute@debian.org>

#enable hardening
export DEB_BUILD_MAINT_OPTIONS=hardening=+all
DEB_COPYRIGHT_CHECK_IGNORE_REGEX = \
	^\./\.git/.*|\./debian/|\./examples/speaker\.gif$

pkglibdir = /usr/lib/pd/extra

%:
	dh $@

override_dh_auto_build:
	dh_auto_build -- \
		arch.flags="" \
		CPPFLAGS="$(CPPFLAGS)" \
		CFLAGS="$(CPPFLAGS) $(CFLAGS)" \
		LDFLAGS="$(LDFLAGS) -Wl,-as-needed" \
		$(empty)

override_dh_auto_install:
	dh_auto_install -- \
		prefix=/usr pkglibdir=$(pkglibdir) \
		STRIP="true" \
		$(empty)
# fix permissions
	find $(CURDIR)/debian/*/$(pkglibdir) -name "*.pd_linux" -exec \
		chmod 0664 {} +
# remove license file so we can replace it with link to the Debian license file
	rm -f -- $(CURDIR)/debian/*/$(pkglibdir)/*/LICENSE.txt

licensecheck:
	licensecheck -i "$(DEB_COPYRIGHT_CHECK_IGNORE_REGEX)" --deb-machine -r . \
		> debian/copyright_newhints
	cmp debian/copyright_hints debian/copyright_newhints \
		&& rm debian/copyright_newhints
